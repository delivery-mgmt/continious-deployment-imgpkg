
## Continuous Deployment with Kapp Controller

For any cluster you will be running any this example on setup the namespace and service account first.

```bash

kubectl apply -f kapp-sa.yaml

```

Once the service account is setup you can begin deploying the *app* resources.

```bash

kubectl apply -f kapp/spring-music-dev-kapp.yaml

```

